module View exposing (view)

import Knight exposing (..)
import Model exposing (..)
import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (..)
import Dict exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import List.Extra exposing (..)


view : Model -> Html Msg
view model =
    div []
        [ input [ placeholder "Rows", Html.Events.onInput Model.Rows ] []
        , input [ placeholder "Cols", Html.Events.onInput Model.Cols ] []
        , button [ Html.Events.onClick Model.StartTour ] [ Html.text "Start Knight's Tour" ]
        , button [ Html.Events.onClick Model.Back ] [ Html.text "Back" ]
        , button [ Html.Events.onClick Model.Stop ] [ Html.text "Stop" ]
        , svg [ version "1.1", x "0", y "0", viewBox "0, 0, 800, 800" ] (render model)
        ]


squareSize =
    50


scaleUp : Int -> Int
scaleUp x =
    (x - 1)
        * squareSize
        + (squareSize // 2)


render : Model -> List (Html Msg)
render model =
    case model.tour of
        NoTour ->
            []

        CompletedTour paths ->
            let
                route =
                    positions paths
            in
                (renderSquares ( model.rows, model.cols ))
                    ++ (renderPositions route)
                    ++ [ renderPath route ]

        ImpossibleTour ->
            (renderSquares ( model.rows, model.cols ))

        Tour (Board extent squares) paths ->
            let
                route =
                    positions paths
            in
                (renderSquares extent)
                    ++ (renderPositions route)
                    ++ [ renderPath route ]


renderSquares : Extent -> List (Html Msg)
renderSquares ( rows, cols ) =
    List.range 1 rows
        |> List.foldl (renderCols cols) []


renderCols : Int -> Int -> List (Html Msg) -> List (Html Msg)
renderCols cols row squares =
    -- let
    --     _ =
    --         Debug.log ("Rendering cols for row " ++ (toString row)) 1
    -- in
    List.range 1 cols
        |> List.foldl (addSquare row) squares


addSquare : Int -> Int -> List (Html Msg) -> List (Html Msg)
addSquare row col squares =
    -- let
    --     _ =
    --         Debug.log ("Addiing squares for row " ++ (toString row) ++ " and col " ++ (toString col)) 1
    -- in
    renderSquare ( row, col ) squares


renderSquare : Position -> List (Html Msg) -> List (Html Msg)
renderSquare ( row, col ) svgList =
    let
        colour =
            (greenOrGrey row col)

        xpos =
            toString ((col - 1) * squareSize)

        ypos =
            toString ((row - 1) * squareSize)
    in
        (rect
            [ fill colour
            , x xpos
            , y ypos
            , Svg.Attributes.width (toString squareSize)
            , Svg.Attributes.height (toString squareSize)
            ]
            []
        )
            :: svgList


renderPath : List Position -> Html Msg
renderPath positions =
    let
        path =
            positions
                |> interject
                |> List.foldl toPathString ""
    in
        polyline [ fill "none", stroke "blue", points path ] []


interject : List Position -> List Position
interject positions =
    let
        end =
            List.Extra.last positions
                |> Maybe.withDefault ( 1, 1 )
    in
        -- i should be cited for ugly AND inefficient code...
        List.map2 addStep positions ((List.drop 1 positions) ++ [ end ])
            |> List.concat


addStep : Position -> Position -> List Position
addStep ( r1, c1 ) ( r2, c2 ) =
    [ ( r1, c1 ), ( r2, c1 ) ]


toPathString : Position -> String -> String
toPathString ( row, col ) points =
    points ++ (toString (scaleUp col)) ++ "," ++ (toString (scaleUp row)) ++ " "


renderPositions : List Position -> List (Html Msg)
renderPositions positions =
    List.map renderPosition positions


renderPosition : Position -> Html Msg
renderPosition ( row, col ) =
    circle
        [ fill "rgba(255, 0, 0, 50)"
        , cx (toString (scaleUp col))
        , cy (toString (scaleUp row))
        , r (toString 10)
        ]
        []


isEven : Int -> Bool
isEven a =
    (a % 2) == 0


greenOrGrey : Int -> Int -> String
greenOrGrey row col =
    if (isEven (row + col)) then
        "rgba(0, 200, 0, 100)"
    else
        "rgba(200, 200, 200, 100)"
