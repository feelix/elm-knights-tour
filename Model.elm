module Model exposing (..)

import Knight exposing (..)
import Html exposing (..)
import Dict exposing (Dict)
import AnimationFrame exposing (..)
import Time exposing (..)


type alias Model =
    { rows : Int
    , cols : Int
    , step : Int
    , elapsed : Float
    , running : Bool
    , path : List Position
    , tour : Tour
    }


type Msg
    = Cycle Time
    | Start
    | StartTour
    | Stop
    | Rows String
    | Cols String
    | Back


model : Model
model =
    Model 0 0 0 0.0 False [] NoTour


init : ( Model, Cmd Msg )
init =
    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.running then
        AnimationFrame.diffs Cycle
    else
        Sub.none


build : Model -> Model
build model =
    { model
        | running = False
        , path = []
        , step = 0
        , elapsed = 0
        , tour = NoTour
    }



-- Model model.rows model.cols solved5by4 (buildBoard model.rows model.cols)
-- buildBoard : Int -> Int -> Board
-- buildBoard rows cols =
--     List.range 1 rows
--         |> List.foldr (addColumnsForRow cols) Dict.empty
-- addColumnsForRow : Int -> Int -> Board -> Board
-- addColumnsForRow cols row board =
--     List.range 1 cols
--         |> List.foldr (addSquare row) board
--
--
-- addSquare : Int -> Int -> Board -> Board
-- addSquare row col board =
--     Dict.insert ( row, col ) NotVisited board
--
--
-- start : Model -> Model
-- start model =
--     model
