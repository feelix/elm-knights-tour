#Elm Plaything
##Knights Tour
###Version 0.0 : Visualisation Framework
This phase just allows for visualising the board and a path of positions taken
on a Knight's tour.
Paths used in the demo come from a similar plaything in Clojure.
Version 1.0 will take on the problem of finding a path, whilst visualising
every move and any back-tracking that happens.
