module Update exposing (update)

import Knight exposing (..)
import Model exposing (..)
import Solutions exposing (..)
import Platform.Cmd exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Start ->
            ( model, Cmd.none )

        -- ( { model
        --     | running = True
        --     , step = 1
        --   }
        --     |> Model.build
        --     |> Model.start
        -- , Cmd.none
        -- )
        StartTour ->
            ( { model
                | running = True
                , tour = tour ( model.rows, model.cols ) ( 1, 1 )
              }
            , Cmd.none
            )

        -- cycle through solved path, one step at a time till done
        Cycle diff ->
            (let
                tour =
                    checkAndStep model.tour
             in
                case tour of
                    Tour board paths ->
                        ( { model
                            | tour = tour
                          }
                        , Cmd.none
                        )

                    NoTour ->
                        ( { model
                            | running = False
                            , tour = tour
                          }
                        , Cmd.none
                        )

                    CompletedTour paths ->
                        ( { model
                            | running = False
                            , tour = tour
                          }
                        , Cmd.none
                        )

                    ImpossibleTour ->
                        ( { model
                            | running = False
                            , tour = tour
                          }
                        , Cmd.none
                        )
            )

        -- if (model.elapsed > 1000.0) then
        --     ( { model
        --         | path = List.take model.step (Solutions.solved8by7)
        --         , step = model.step + 1
        --         , elapsed = 0.0
        --       }
        --     , Cmd.none
        --     )
        -- else
        --     ( { model | elapsed = model.elapsed + diff }, Cmd.none )
        Back ->
            ( { model | step = model.step - 1 }, Cmd.none )

        Stop ->
            ( { model | running = not model.running }, Cmd.none )

        Rows value ->
            ( { model | rows = Result.withDefault 0 (String.toInt value) }, Cmd.none )

        Cols value ->
            ( { model | cols = Result.withDefault 0 (String.toInt value) }, Cmd.none )
