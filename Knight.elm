module Knight exposing (..)

-- import Model exposing (..)

import Maybe exposing (..)
import Dict exposing (..)
import List exposing (..)
import List.Extra exposing (..)
import Set exposing (..)


type Tour
    = Tour Board Paths
    | NoTour
    | ImpossibleTour
    | CompletedTour Paths


type alias Paths =
    List ( Position, Moves )



-- = Paths (List ( Position, Moves ))
-- | NoPaths


type alias Moves =
    List Move



-- = Moves (List Move)
-- | NoMoves


type alias Move =
    ( Int, Int )


type alias Squares =
    Set Position


type Board
    = Board Extent Squares


type alias Position =
    ( Int, Int )


type alias Extent =
    ( Int, Int )


{-| create a tour starting at position on a board of extent
eg tour (8, 8) (1, 1)
-}
tour : Extent -> Position -> Tour
tour extent position =
    let
        board =
            Board extent Set.empty
                |> occupy position

        moves =
            nextValidMovesByDegreesOfFreedom position board

        paths =
            [ ( position, moves ) ]
    in
        Tour board paths


{-| take next step in the Tour, and check for terminated tour
-}
checkAndStep : Tour -> Tour
checkAndStep tour =
    case tour of
        NoTour ->
            tour

        ImpossibleTour ->
            tour

        CompletedTour paths ->
            tour

        Tour board paths ->
            step board paths
                |> checkCompleted


{-| if all squares have been occupied Tour becomes CompletedTour
-}
checkCompleted : Tour -> Tour
checkCompleted tour =
    case tour of
        Tour (Board ( rows, cols ) squares) paths ->
            if ((rows * cols) == (List.length paths)) then
                CompletedTour paths
            else
                tour

        _ ->
            tour


{-| take top move stored at current path position
    if no path available then we've backtracked beyond starting point so
    this must be an impossible Tour
-}
step : Board -> Paths -> Tour
step board paths =
    case
        path paths
    of
        Nothing ->
            ImpossibleTour

        Just ( position, moves ) ->
            makeNextMove position moves board paths


{-|
-}
makeNextMove : Position -> Moves -> Board -> Paths -> Tour
makeNextMove position moves board paths =
    case nextMove moves of
        -- occupy new position and add new (position, validMoves to path)
        Just move ->
            let
                dest =
                    destination position move
            in
                Tour
                    (occupy dest board)
                    (( dest, nextValidMovesByDegreesOfFreedom dest board ) :: paths)

        -- no move avaiable, have to go back in paths and unoccupy position
        -- DONT call step if want to see it backtracking
        Nothing ->
            Tour
                (unoccupy position board)
                (backtrack paths)


backtrack : Paths -> Paths
backtrack paths =
    paths
        |> List.tail
        |> Maybe.map popMove
        |> Maybe.withDefault []


popMove : Paths -> Paths
popMove paths =
    case paths of
        ( position, moves ) :: rest ->
            ( position, Maybe.withDefault [] (List.tail moves) )
                :: rest

        [] ->
            paths


path : Paths -> Maybe ( Position, Moves )
path paths =
    List.head paths


allMoves : List Move
allMoves =
    [ ( 2, 1 ), ( 2, -1 ), ( -2, 1 ), ( -2, -1 ), ( -1, 2 ), ( 1, -2 ), ( -1, -2 ), ( 1, -2 ), ( 1, 2 ) ]


destination : Position -> Move -> Position
destination ( row, col ) ( r, c ) =
    ( row + r, col + c )


isOnboard : Extent -> Position -> Bool
isOnboard ( rows, cols ) ( row, col ) =
    (row <= rows) && (col <= cols) && (row > 0) && (col > 0)


isUnoccupied : Squares -> Position -> Bool
isUnoccupied squares position =
    not (Set.member position squares)


isValidMove : Position -> Board -> Move -> Bool
isValidMove position (Board extent squares) move =
    let
        dest =
            destination position move
    in
        (isOnboard extent dest) && (isUnoccupied squares dest)


{-| initially set all possible valid moves from position
-}
validMoves : Position -> Board -> Moves
validMoves position board =
    allMoves
        |> List.filter (isValidMove position board)


{-| Warnorfs 'rule' say go for move with fewest degreees
    of freedom for the next move
-}
nextValidMovesByDegreesOfFreedom : Position -> Board -> Moves
nextValidMovesByDegreesOfFreedom position board =
    validMoves position board
        |> List.map (\move -> ( move, score (destination position move) board move ))
        |> List.sortBy (\( move, score ) -> score)
        |> List.map (\( move, score ) -> move)



-- |> List.reverse


score : Position -> Board -> Move -> Int
score position board move =
    validMoves position board
        |> List.length


unoccupy : Position -> Board -> Board
unoccupy position (Board extent squares) =
    let
        _ =
            (if (position == ( 1, 1 )) then
                Debug.log "unoccupying start square!!!!" 1
             else
                1
            )
    in
        Board extent (Set.remove position squares)


occupy : Position -> Board -> Board
occupy position (Board extent squares) =
    Board extent (Set.insert position squares)


nextMove : Moves -> Maybe Move
nextMove moves =
    List.head moves


positions : Paths -> List Position
positions paths =
    List.map (\( p, m ) -> p) paths
